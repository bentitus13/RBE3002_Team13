#!/usr/bin/env python
import rospy
from nav_msgs.msg import GridCells
from std_msgs.msg import String
from geometry_msgs.msg import Twist, Point, Pose, PoseStamped, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry, OccupancyGrid
from kobuki_msgs.msg import BumperEvent
import tf
import numpy
import math
import rospy, tf, numpy, math
from lab3_grid_cells import GridCellPub
import astar
from Robot import Robot
from tf.transformations import quaternion_from_euler

#Main handler of the project
def run():
    #init() # idk man
    rospy.init_node('lab4')

    wheel_base = 50.0/100.0 #centimeters

    mrRoboto = Robot(wheel_base)
    theGrid = GridCellPub()
    theGrid.__init__()
    rospy.sleep(2) #allowing time for publishers, subscribers, etc.
    theGrid.addBuffer(wheel_base)
    rospy.sleep(1)
    # print("waiting for the start and end")

    while not theGrid._flag1 or not theGrid._flag2:
        pass

    # print ("initial pose and nav goal saved")
    robXpos, robYpos = theGrid.getStartPos()
    print "x " + str(robXpos) + " y " + str(robYpos)


    start = astar.Node(robXpos, robYpos)
    goalX = theGrid._goalX
    goalY = theGrid._goalY

    print "x " + str(goalX) + " y " + str(goalY)
    rospy.sleep(2)
    goal = astar.Node(goalX, goalY)
    # start = astar.Node(0, 0)
    # goal = astar.Node(219, 206)
    path = start.AswoleStar(goal, theGrid, mrRoboto._wheelbase, .97)
    curWaypointPath = start.wayPointCalculator(path, theGrid)

    while not rospy.is_shutdown():
        rospy.sleep(2)
        driveList = []
        updateWaypointPath = []

        place = curWaypointPath[1]
        stamp = PoseStamped()
        stamp.header.frame_id = "map"
        stamp.header.stamp = rospy.Time()
        node = place[0]
        angle = place[1]
        pose = Pose()
        pose.position.x = (node._x_pos * theGrid._resolution) + theGrid._offsetX + (.5*theGrid._resolution)
        pose.position.y = (node._y_pos * theGrid._resolution) + theGrid._offsetY + (.5*theGrid._resolution)
        quat = quaternion_from_euler(0,0,angle)
        pose.orientation.x = quat[0]
        pose.orientation.y = quat[1]
        pose.orientation.z = quat[2]
        pose.orientation.w = quat[3]
        stamp.pose = pose
        driveList.append(stamp)
        # theGrid.pubNode(node,"test")
        # rospy.sleep(2)
        mrRoboto.navToPose(stamp)
        rospy.sleep(2)
        # try:
        updatePath = node.AswoleStar(goal, theGrid, mrRoboto._wheelbase,.97)
        updateWaypointPath = node.wayPointCalculator(updatePath, theGrid)
        # except IndexError:
        #     pass

    # for stamp in driveList:
    #     mrRoboto.navToPose(stamp)
    #     # drive.publish(stamp)
    #     rospy.sleep(2)

def test_navToPose():
    wheel_base = 23.0/100.0 #centirmeters

    mrRoboto = Robot(wheel_base)

    print "Click around to make the robot drive"

    while not rospy.is_shutdown():
        pass

def test_driveStraight():
    drive = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size=1)
    # run()
    wheel_base = 23.0 / 100.0  # centimeters
    domoArigato = Robot(wheel_base)
    domoArigato.driveStraight(0.2,5)


if __name__ == '__main__':
    rospy.init_node('lab4')
    try:

        run()
        # wheel_base = 23.0 / 100.0  # centimeters
        # domoArigato = Robot(wheel_base)
        # domoArigato.rotate(0.5)
        # test_navToPose()
    except rospy.ROSInterruptException:
        pass
