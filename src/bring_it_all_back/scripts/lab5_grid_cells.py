#!/usr/bin/env python
import rospy
from nav_msgs.msg import GridCells
from std_msgs.msg import String
from geometry_msgs.msg import Twist, Point, Pose, PoseStamped, PoseWithCovarianceStamped
from nav_msgs.msg import Odometry, OccupancyGrid
from kobuki_msgs.msg import BumperEvent
import rospy, tf, numpy, math
from tf.transformations import euler_from_quaternion


class GridCellPub:
    def __init__(self):
        self._sub = rospy.Subscriber("/map", OccupancyGrid, self.mapCallBack)
        self._pub = rospy.Publisher("/map_check", GridCells, queue_size=1)
        self._known = rospy.Publisher("/known", GridCells, queue_size=1)
        self._unknown = rospy.Publisher("/unknown", GridCells, queue_size=1)
        self._pubpath = rospy.Publisher("/path", GridCells, queue_size=1)
        self._pubway = rospy.Publisher("/waypoints", GridCells, queue_size=1)
        self._goal_sub = rospy.Subscriber('move_base_simple/goal3', PoseStamped, self.readGoal, queue_size=1)
        self._goal_sub1 = rospy.Subscriber('initialpose', PoseWithCovarianceStamped, self.readStart, queue_size=1)
        self._AstarPath = rospy.Publisher("/AstarPath", GridCells, queue_size=1)
        self._frontier = rospy.Publisher("/frontier", GridCells, queue_size=1) #openset
        self._expanded = rospy.Publisher("/expanded", GridCells, queue_size=1) #closedset
        self._current = rospy.Publisher("/current", GridCells, queue_size=1) #closedset
        self._buff_pub = rospy.Publisher("/buffer", GridCells, queue_size=1)
        self._costMap = rospy.Subscriber("move_base/local_costmap/costmap", OccupancyGrid, self.costmapCallBack)
        self._cluster = rospy.Publisher("/cluster", GridCells, queue_size=1)
        self._localFrontier = rospy.Publisher("/local", GridCells, queue_size=1)
        self._bigCluster = rospy.Publisher("/bigCluster", GridCells, queue_size=1)
        self._swoleFrontier = rospy.Publisher("/swoleFrontier", GridCells, queue_size=1)
        self._flag1 = False
        self._flag2 = False


    def clearRVIZ(self):
        cell = GridCells()

        cell.header.frame_id = '/map'
        cell.cell_width = self._resolution
        cell.cell_height = self._resolution

        self._pub.publish(cell)
        self._known.publish(cell)
        self._unknown.publish(cell)
        self._pubpath.publish(cell)
        self._pubway.publish(cell)
        self._AstarPath.publish(cell)
        self._frontier.publish(cell)
        self._expanded.publish(cell)
        self._current.publish(cell)
        self._buff_pub.publish(cell)
        self._cluster.publish(cell)
        self._localFrontier.publish(cell)
        self._bigCluster.publish(cell)
        self._swoleFrontier.publish(cell)

    def getStartPos(self):
        return [self._startPosX, self._startPosY, self._startAngle]

    # reads in self._map
    def mapCallBack(self, data):
        self._mapgrid = data
        self._resolution = data.info.resolution
        self._mapData = data.data
        self._width = data.info.width
        self._height = data.info.height
        self._offsetX = data.info.origin.position.x
        self._offsetY = data.info.origin.position.y
        # print data.info

    #reads in self._costMap
    def costmapCallBack(self, data):
        self._costMapgrid = data
        self._costMapresolution = data.info.resolution
        self._costMapData = data.data
        self._costMapwidth = data.info.width
        self._costMapheight = data.info.height
        self._costMapoffsetX = data.info.origin.position.x
        self._costMapoffsetY = data.info.origin.position.y

    def readGoal(self, goal):
        self._goalX= int((goal.pose.position.x - self._offsetX - (.5 * self._resolution)) / self._resolution)
        self._goalY= int((goal.pose.position.y - self._offsetY - (.5 * self._resolution)) / self._resolution)
        self._goalAngle = self.findYaw(goal.pose.orientation)
        self._flag2 = True
        return goal.pose

    def readStart(self, startPos):
        self._startPosX = int((startPos.pose.pose.position.x - self._offsetX - (.5 * self._resolution)) / self._resolution)
        self._startPosY = int((startPos.pose.pose.position.y - self._offsetY- (.5 * self._resolution)) / self._resolution)
        self._startAngle = self.findYaw(startPos.pose.pose.orientation)
        # print startPos.pose.pose
        self._flag1 = True
        return startPos.pose.pose

    def pubNode(self, nodelist, style):
        cell = GridCells()

        cell.header.frame_id = '/map'
        cell.cell_width = self._resolution
        cell.cell_height = self._resolution

        #determine if we have one input or multiple
        # try to determine the length of the input
        # if isinstance(nodelist,list):
        #     isList = True
        # try:
        #     nodelist._x_pos
        # except AttributeError:
        #     # if we get this exception, we know we didn't get a node input
        #     isNode = False

        if isinstance(nodelist,list) or isinstance(nodelist, set):
            for node in nodelist:
                if style == "swoleFrontier":
                    node = node[1]
                point = Point()
                point.x = (node._x_pos * self._resolution) + self._offsetX + (.5 * self._resolution)
                point.y = (node._y_pos * self._resolution) + self._offsetY + (.5 * self._resolution)
                point.z = 0
                cell.cells.append(point)
        else:
            point = Point()
            point.x = (nodelist._x_pos * self._resolution) + self._offsetX + (.5 * self._resolution)
            point.y = (nodelist._y_pos * self._resolution) + self._offsetY + (.5 * self._resolution)
            point.z = 0
            cell.cells.append(point)

        if style == "current":
            # use the current publisher
            self._current.publish(cell)
            # print ("publishing current")

        # elif style == "test":
            # use the current publisher
            # print ("publishing test")

        elif style == "frontier":
            # use the frontier publisher
            self._frontier.publish(cell)
            # print ("publishing frontier")

        elif style == "closedSet":
            # use the closed set publisher
            self._expanded.publish(cell)
            # print ("publishing expanded cells")

        elif style == "path":
            # use the path publisher
            self._AstarPath.publish(cell)
            # print("publishing A*")

        elif style == "waypoint":
            # use the waypoint publisher
            self._pubway.publish(cell)
            # print("publishing waypoint")

        elif style == "cluster":
            # use the cluster publisher
            self._cluster.publish(cell)

        elif style == "local":
            # use the cluster publisher
            self._localFrontier.publish(cell)

        elif style == "bigCluster":
            # use the cluster publisher
            self._bigCluster.publish(cell)

        elif style == "swoleFrontier":
            #use the swoleFrontier publisher
            self._swoleFrontier.publish(cell)

    # saves values of cells in cost map
    def getCostMap(self):

        k = 0

        costMapCells=[]

        for i in range(0,self._costMapheight):
            for j in range(0,self._costMapwidth):
                if (self._costMapData[k] >= 0): #if greater than unknown (-1), save the value
                    costMapCells.append(self._costMapData[k])
                elif (self._costMapData[k] == -1):
                    costMapCells.append(0) #don't make the cost to go there lower, just don't change it
                k=k+1
        return costMapCells


    def publishCells(self):
        # print "publishing occupied cells"
        # resolution and offset of the map
        k = 0
        cells = GridCells()
        cells.header.frame_id = 'map'
        cells.cell_width = self._resolution
        cells.cell_height = self._resolution

        occupiedCells=[]

        for i in range(0,self._height): #height should be set to height of grid
            for j in range(0,self._width): #width should be set to width of grid
                #print k # used for debugging
                if (self._mapData[k] == 100):
                    # this point is occupied (x,y)=(j,i)
                    # record this point as an occupied cell
                    tempNode = (j,i)
                    occupiedCells.append(tempNode)
                    # publish this point to rviz
                    point=Point()
                    point.x = (j*self._resolution)+self._offsetX + (.5 * self._resolution)  # added secondary offset
                    point.y = (i*self._resolution)+self._offsetY + (.5 * self._resolution)  # added secondary offset
                    point.z = 0
                    cells.cells.append(point)
                k=k+1
                # print('y= ',i,' x= ',j)
        # run A* on the grid
        self._pub.publish(cells)
        cellsNew = numpy.reshape(self._mapData,(self._width,self._height))
        return occupiedCells


    def addBuffer(self,diameter):
        occupiedCells=self.publishCells()
        diameter = (diameter) / self._resolution
        # print('diameter -->', diameter)
        radius = int(math.ceil(diameter / 2.0))
        radius = int(diameter/2.0 + 0.5)
        # print('radius -->',radius)
        cells = GridCells()
        cells.header.frame_id = 'map'
        cells.cell_width = self._resolution
        cells.cell_height = self._resolution

        # bufferZone = [[0]*self._width]*self._height # Make huge 2D array of zeros woooohooooo
        bufferZone = {(i, j): 0 for i in range(self._width) for j in range(self._height)}
        # print('buff-->',bufferZone)
        for node in occupiedCells:
            tempx = node[0]
            tempy = node[1]
            # print('tempx-->', tempx, 'tempy-->', tempy)
            for i in range(0,radius+1):
                for j in range(0,radius+1):
                    # if we are about to go out of bounds, break out
                    # if (tempy+j>self._height):
                    #     break

                    try:
                        bufferZone[(tempx + i),(tempy + j)] = 100 #all buffer cells in top right corner
                        bufferZone[(tempx - i),(tempy + j)] = 100 #all buffer cells in top left corner
                        bufferZone[(tempx + i),(tempy - j)] = 100 #all buffer cells in the bottom right
                        bufferZone[(tempx - i),(tempy - j)] = 100 #all the buffer cells in the bottom left
                        pass
                    except IndexError:
                        pass
        occupiedBuff=[]
        for i in range(0,self._width): # Height should be set to height of grid
            for j in range(0,self._height): # Width should be set to width of grid

                try:
                    if (bufferZone[i,j] == 100):
                        # This point is occupied (x,y)=(i,j)
                        # Record this point as an occupied cell
                        tempNode = (i,j)
                        occupiedBuff.append(tempNode)
                        # Publish this point to rviz
                        point=Point()
                        # convert from 2D array to grid cells
                        point.x = (i*self._resolution)+self._offsetX + (.5 * self._resolution)  # added secondary offset
                        point.y = (j*self._resolution)+self._offsetY + (.5 * self._resolution)  # added secondary offset
                        point.z = 0
                        cells.cells.append(point)
                    else:
                        bufferZone[i,j] = 0
                except KeyError:
                    pass

        self._buff_pub.publish(cells)
        self._buffer = occupiedBuff
        return occupiedBuff

    def findYaw(self,quaternion):
        q = [quaternion.x,quaternion.y,quaternion.z,quaternion.w]
        (roll, pitch, yaw) = euler_from_quaternion(q)
        return(yaw)

    # inputs: x,y coordinates in grid space
    # output: x,y in meters
    # purpose: convert between grid space and meters
    def gridToMeters(self,x,y):
        # print('coords about to be converted: x-> ', x, ' y-> ', y)
        outX = (x * self._resolution) + self._offsetX + (.5 * self._resolution)  # added secondary offset
        outY = (y * self._resolution) + self._offsetY + (.5 * self._resolution)  # added secondary offset
        # print('freshly converted to meters: x-> ', outX, 'y-> ', outY)
        return [outX,outY]

    # inputs: x,y coordinates in meters
    # output: Point in grid space
    # purpose: convert between meters and grid space
    def meterToGrid(self,x,y):
        # print('coords about to be converted: x-> ',x,' y-> ',y)
        gridX = int((x - self._offsetX - (.5 * self._resolution)) / self._resolution)
        gridY = int((y - self._offsetY - (.5 * self._resolution)) / self._resolution)
        # print('freshly converted to grid cells: x-> ',gridX,'y-> ',gridY)
        return [gridX,gridY]

